<?php

namespace Drupal\missplete\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Select;
use Drupal\missplete\Plugin\Field\FieldWidget\MisspleteFieldWidget;

/**
 * Class Missplete.
 *
 * @package Drupal\missplete\Render\Element
 *
 * @RenderElement("missplete")
 */
class Missplete extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    $info = parent::getInfo();
    $info['#process'][] = [$class, 'processMissplete'];
    $info['#pre_render'] = [
      [$class, 'preRenderMissplete'],
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = parent::valueCallback($element, $input, $form_state);
    if (isset($value['select']) && !empty($value['select'])) {
      return $value['select'];
    }
    else {
      return NULL;
    }
  }

  /**
   * Processes callback.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @see _form_validate()
   */
  public static function processMissplete(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $name = $element['#name'];

    // Make sure placeholder attribute is only applied on the input field.
    $input_attributes = $element['#attributes'] + ['autocomplete' => 'off'];
    unset($element['#attributes']['placeholder']);

    // Get select options.
    $options = [];
    if (isset($element['#options']) && is_array($element['#options'])) {
      $options = $element['#options'];
    }

    $element['select'] = [
      '#name' => $name . '[select]',
      '#type' => 'select',
      '#title' => $element['#title'],
      '#title_display' => $element['#title_display'],
      '#options' => $options,
      '#required' => $element['#required'],
      '#attributes' => $element['#attributes'],
      '#wrapper_attributes' => ['class' => 'form-type-missplete-select'],
    ];
    $element['input'] = [
      '#input' => FALSE,
      '#name' => $name . '[input]',
      '#type' => 'textfield',
      '#title' => $element['#title'],
      '#title_display' => $element['#title_display'],
      '#required' => $element['#required'],
      '#attributes' => $input_attributes,
      '#wrapper_attributes' => ['class' => 'form-type-missplete-input'],
    ];

    // If required then take first element so validation does not fail.
    if ($element['#required'] && empty($element['#value'])) {
      reset($options);
      $element['select']['#default_value'] = key($options);
    }

    // Set default values for input and select fields.
    if (isset($element['#default_value'][0]) && !empty($element['#default_value'][0])) {
      $element['select']['#default_value'] = $element['#default_value'][0];
      $element['input']['#value'] = $options[$element['#default_value'][0]];
    }

    $score = MisspleteFieldWidget::SCORE;
    if (isset($element['#score'])) {
      $score = $element['#score'];
    }

    $element['#attached']['library'][] = 'missplete/drupal.missplete';
    $element['#attached']['drupalSettings']['missplete'][$name] = [
      'options' => $options,
      'score' => (float) $score,
    ];

    $element['#title'] = '';
    $element['#theme'] = 'missplete';
    $element['#required'] = FALSE;
    return $element;
  }

  /**
   * Prepares a missplete render element.
   */
  public static function preRenderMissplete($element) {
    Element::setAttributes($element, ['id', 'name']);
    static::setAttributes($element, ['form-missplete']);
    return $element;
  }

}
