<?php

namespace Drupal\missplete\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'missplete_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "missplete_field_widget",
 *   label = @Translation("Missplete"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class MisspleteFieldWidget extends OptionsSelectWidget {

  /**
   * Minimum score required for options to show up.
   */
  const SCORE = 0.75;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'score' => self::SCORE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['score'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum score'),
      '#description' => $this->t('Minimum score required for options to show up.'),
      '#default_value' => $this->getSetting('score'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [
      $this->t('Minimum score: @score', ['@score' => $this->getSetting('score')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#type'] = 'missplete';
    $element['#score'] = $this->getSetting('score');
    return $element;
  }

}
