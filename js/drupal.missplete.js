/**
 * @file
 * Attaches behaviors for Missplete integration.
 */
(function (Drupal, drupalSettings, MissPlete, _) {

  'use strict';

  /**
   * Enable missplete for given fields.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.activeLinks = {
    attach: function (context) {
      _.each(drupalSettings.missplete, function (settings, name) {
        var elements;
        var i;

        // Hide select field on all missplete elements.
        elements = document.getElementsByClassName("form-type-missplete-select");
        for(i = 0; i < elements.length; i++){
          elements[i].style.display = "none";
        }

        // Show autocomplete input field on all missplete elements.
        elements = document.getElementsByClassName("form-type-missplete-input");
        for(i = 0; i < elements.length; i++){
          elements[i].style.display = "block";
        }

        // Prevent form to be submitted on enter.
        document.querySelector('.form-type-missplete-input').onkeypress = function (e) {
          return (e.keyCode || e.which || e.charCode || 0) !== 13;
        };

        document.querySelector('.form-type-missplete-input input').addEventListener('input', function (e) {
          // Remove duplicate miss-plete div.
          var dropdown = document.querySelectorAll('.miss-plete');
          for (var i in dropdown) {
            if (dropdown[i].nodeType) {
              dropdown[i].parentNode.removeChild(dropdown[i]);
            }
          }

          if (!this.value.length) {
            // Set value to _none when the input value is empty.
            // @todo replace _none with null once Drupal default select option value will be empty.
            document.querySelector('.form-type-missplete-select select').value = '_none';
          }
        });

        // Retrieve options.
        var options = _.map(settings.options, function (value, key) {
          return [value, key];
        });

        // Initialize autocomplete.
        new MissPlete.default({
          input: document.querySelector('input[name="' + name + '[input]"]'),
          options: options,
          selectFn: function (option) {
            if (option) {
              var selected = _.find(this.options, function (item) {
                return item[0] == this.displayValue;
              }, option);

              // Change option on select field.
              var select = document.querySelector('select[name="' + name + '[select]"]');
              for (var i, j = 0; i = select.options[j]; j++) {
                if (i.value == selected[1]) {
                  select.selectedIndex = j;

                  // Define and dispatch custom event event.
                  var event = document.createEvent('Event');
                  event.initEvent('missplete.item_selected', true, true);
                  event.selectedItem = { label: selected[0], value: selected[1] };
                  document.dispatchEvent(event);
                  break;
                }
              }

              // Set new input value and remove dropdown.
              this.input.value = selected[0];
              this.removeDropdown();
            }
          },
          // Overwrite the minimum score.
          listItemFn: function (scoredOption, itemIndex) {
            var li = scoredOption.score < settings.score ? null : document.createElement("li");
            li && li.appendChild(document.createTextNode(scoredOption.displayValue));
            return li;
          }
        });

      }, this);
    }
  };

})(Drupal, drupalSettings, MissPlete, _);
