# MissPlete

Drupal integration for [MissPlete](http://xavi.github.io/miss-plete/):
a misspelling-tolerant autocomplete in less than 220 lines, no-dependencies.

## Installation
The module requires a MissPlete fork available [here](https://github.com/nuvoleweb/miss-plete/tree/lashab/master).

As specified in `missplete.libraries.yml` the module expects to find the
required JavaScript library at `/libraries/miss-plete/dist/bundle.js`.

Please add code below to your composer.json file in "repository" section in order to install nuvoleweb/miss-plete js library through composer:

```yml
{
    "type": "package",
    "package": {
        "name": "nuvoleweb/miss-plete",
        "version": "1.4.3",
        "type": "drupal-library",
        "dist": {
            "url": "https://github.com/nuvoleweb/miss-plete/archive/e56a72897fd98421ce122cccfc051f191c014793.zip",
            "type": "zip"
        }
    }
}
```
